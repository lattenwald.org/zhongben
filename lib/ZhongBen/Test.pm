package ZhongBen::Test;
use strict;
use warnings;

use Data::Dumper;

use Mojo::Base 'Mojolicious::Controller';
use Mojo::Util qw/trim/;

sub replace_map {
	my $self = shift;
	$self->render(
		text => join( ",\n", map "$_ => $ZhongBen::Model::replace_map{$_}", sort keys %ZhongBen::Model::replace_map ),
		format => 'txt',
	);
}

1;
