package ZhongBen::Util;
use strict;
use warnings;

use Exporter qw/import/;
use utf8;

BEGIN {
	our @EXPORT    = qw//;
	our @EXPORT_OK = qw/trim/;
}

my %accents_map = (
	iao => 'ia*o',
	uai => 'ua*i',
	ai  => 'a*i',
	ao  => 'a*o',
	ei  => 'e*i',
	ia  => 'ia*',
	ie  => 'ie*',
	io  => 'io*',
	iu  => 'iu*',
	Ai  => 'A*i',
	Ao  => 'A*o',
	Ei  => 'E*i',
	ou  => 'o*u',
	ua  => 'ua*',
	ue  => 'ue*',
	ui  => 'ui*',
	uo  => 'uo*',
	ve  => 'üe*',
	Ou  => 'O*u',
	a   => 'a*',
	e   => 'e*',
	i   => 'i*',
	o   => 'o*',
	u   => 'u*',
	v   => 'v*',
	A   => 'A*',
	E   => 'E*',
	O   => 'O*',
);

my %pinyin = (
	a => [qw/ā á ǎ à a/],
	e => [qw/ē é ě è e/],
	i => [qw/ī í ǐ ì i/],
	o => [qw/ō ó ǒ ò o/],
	u => [qw/ū ú ǔ ù u/],
	v => [qw/ǖ ǘ ǚ ǜ ü/],
	A => [qw/Ā Á Ǎ À A/],
	E => [qw/Ē É Ě È E/],
	I => [qw/Ī Í Ǐ Ì I/],
	O => [qw/Ō Ó Ǒ Ò O/],

);

{
	my %replace_map = map {
		my $k = $_;
		map { ( $pinyin{$k}[$_] => "$k$_" ) } 0 .. $#{ $pinyin{$k} }
	} keys %pinyin;

	my $replace_what = '[' . join( '', keys %replace_map ) . ']';

	sub pinyin_sort {
		my ( $a, $b ) = map lc, @_;
		s/($replace_what)/$replace_map{$1}/g for $a, $b;
		$a cmp $b;
	}
}

{
	my $replace_what = '(' . join( '|', reverse sort { length($a) <=> length($b) } keys %accents_map ) . ')';

	sub pin1yin1_display {
		my $str = shift;
		$str = shift if ref($str);
		my @str;

		while ( $str =~ /([^\d]+)([1-5])/ ) {
			$str = $';
			push @str, $` if $`;
			my $tone = $2;
			( my $s = $1 ) =~ s/$replace_what(.*?)$/$accents_map{$1}$2/;
			$s =~ s/(.)\*/$pinyin{$1}[$tone-1]/;
			push @str, $s;
		}
		push @str, $str;
		return join( '', @str );
	}
}

sub trim {
	local $_ = shift;
	s/^\s+//;
	s/\$+$//;
	return $_;
}

1;
