package ZhongBen::Model;
use strict;
use warnings;
use utf8;
use fields qw/dbh db_connection/;

use DBI;
use DBD::SQLite;
use ZhongBen::Util;

$DBD::SQLite::COLLATION{pin1yin1} = \&ZhongBen::Util::pinyin_sort;

sub AUTOLOAD {
	our $AUTOLOAD;
	my $self = shift;
	my $call = $AUTOLOAD;
	$call =~ s/.*:://;

	$self->connect unless $self->ping;

	return $self->{dbh}->$call(@_);
}

sub new {
	my ( $self, $db_connection ) = @_;
	$self = fields::new($self) unless ref $self;
	$self->{db_connection} = $db_connection;
	return $self;
}

sub connect {
	my $self = shift;
	$self->{dbh} = DBI->connect( @{ $self->{db_connection} } );
	$self->{dbh}->{sqlite_unicode} = 1;
	$self->{dbh}->do("PRAGMA foreign_keys = ON");
	return $self;
}

sub ping {
	my $self = shift;
	return $self->{dbh} && $self->{dbh}->selectrow_array("SELECT 1");
}

sub get_tags_tree {
	my ( $self, $word_id ) = @_;
	$self->connect unless $self->ping;
	my $tags = $self->{dbh}->selectall_hashref( "SELECT * FROM tags ORDER BY parent, name", 'id' );

	if ($word_id) {
		my $tags_on = $self->{dbh}->selectcol_arrayref( "SELECT tag FROM tags_words WHERE word=?", undef, $word_id );
		$tags->{$_}{on} = 1 for @$tags_on;
	}

	for my $id ( keys %$tags ) {
		my @children_ids =
			map { $_->{id} }
			grep { $_->{parent} and $_->{parent} == $id }
			sort { ( $a->{parent} || 0 ) <=> ( $b->{parent} || 0 ) or $a->{name} cmp $b->{name} } values %$tags;
		$tags->{$id}{children} = [ @$tags{@children_ids} ];
	}

	return [ grep { not $_->{parent} } values %$tags ];
}

1;
