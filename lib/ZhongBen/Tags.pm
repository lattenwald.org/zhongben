package ZhongBen::Tags;
use strict;
use warnings;

use Mojo::Base 'Mojolicious::Controller';
use Mojo::Util qw/trim/;

sub new_tag {
	my $self = shift;

	my $tag_name = $self->param('tag_name');
	$self->app->log->debug("new tag $tag_name");
	my $tag_id = $self->dbh->selectrow_array( "SELECT id FROM tags WHERE name=?", undef, $tag_name );
	unless ($tag_id) {
		$self->dbh->do( "INSERT INTO tags(name) VALUES (?)", undef, $tag_name );
		$tag_id = $self->dbh->last_insert_id( '', '', '', '' );
	}
	$self->render( json => { tag_id => $tag_id, tag_name => $tag_name } );
}

sub show_tags_tree {
	my $self = shift;
	my $tree = $self->dbh->get_tags_tree;
	$self->render( 'tags/tree', tags_tree => $tree, w => undef );
}

sub tag_dropped {
	my $self = shift;

	trim( my $what  = $self->param('what') );
	trim( my $where = $self->param('where') );

	my $tags =
		$self->dbh->selectall_hashref( "SELECT * FROM tags WHERE name in (?, ?)", 'name', undef, $what, $where );
	$self->app->log->debug( sprintf "What: %s, Where: %s", $what, $where );
	$self->dbh->do(
		"UPDATE tags SET parent=? WHERE parent=?",
		undef,
		$tags->{$what}->{parent},
		$tags->{$what}->{id}
	);
	$self->dbh->do( "UPDATE tags SET parent=? WHERE id=?", undef, $tags->{$where}->{id}, $tags->{$what}->{id} );

	$self->render( json => { what => $what, where => $where, result => 'done' } );
}

1;
