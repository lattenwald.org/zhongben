package ZhongBen::Words;
use strict;
use warnings;
use utf8;

use Mojo::Base 'Mojolicious::Controller';
use Mojo::Util qw/trim/;
use Mojo::Template;
use Mojolicious::Static;
use File::Temp 'tempfile';
use DateTime;

sub list {
	my $self = shift;

	my $tag_id = $self->param('tag');
	my $page   = $self->param('page') || 1;
	my $rows   = $self->param('rows') || 50;

	my $page_sql = '';
	if ( $page ne 'all' ) {
		$page_sql = sprintf " LIMIT %d OFFSET %d", $rows, ( $page - 1 ) * $rows;
	}

	my $sql = "SELECT COUNT(DISTINCT(words.id)) FROM words";
	if ($tag_id) {
		$sql .= " JOIN tags_words tw ON tw.word = words.id WHERE tw.tag = " . $self->dbh->quote($tag_id);
	}
	my $words_cnt   = $self->dbh->selectrow_array($sql);
	my $total_pages = int $words_cnt / $rows;
	$total_pages++ if $words_cnt % $rows;
	my $query = $self->url_for('words_list')->query(
		page => $page,
		( $rows != 50 ? ( rows => $rows ) : () ), ( $tag_id ? ( tag => $tag_id ) : () )
	);
	if ( $page ne 'all' and $page > $total_pages ) {
		$self->redirect_to( $query->query( page => $total_pages ) );
	}

	$sql = "SELECT words.* FROM words";
	if ($tag_id) {
		$sql .= " JOIN tags_words tw ON tw.word = words.id WHERE tw.tag = " . $self->dbh->quote($tag_id);
	}
	$sql .= " ORDER BY pinyin";
	$sql .= $page_sql;
	my $words = $self->dbh->selectall_arrayref( $sql, { Slice => {} } );

	$self->stash(
		words_cnt   => $words_cnt,
		words       => $words,
		start_num   => ( $page eq 'all' ? 1 : ( ( $page - 1 ) * $rows + 1 ) ),
		page        => $page,
		rows        => $rows,
		tag         => $tag_id,
		total_pages => $total_pages,
		query       => $query,
		tags_tree   => $self->dbh->get_tags_tree,
		w           => undef,
	);
}

sub show_list {
	my $self             = shift;
	my $pdf_sample_query = $self->url_for('pdf_sample')->query(
		page => $self->stash->{page},
		( $self->stash->{rows} != 50 ? ( rows => $self->stash->{rows} ) : () ),
		( $self->stash->{tag}        ? ( tag  => $self->stash->{tag} )  : () )
	);
	$self->render( 'words/list', pdf_sample_query => $pdf_sample_query );
}

sub pdf_sample {
	my $self = shift;

	my @hanzi = map { split //, $_->{hanzi} } @{ $self->stash->{words} };
	my %hanzi = map { ( $_ => 1 ) } @hanzi;
	@hanzi = sort keys %hanzi;

	my $mt  = Mojo::Template->new;
	my $tpl = 'templates/svg/sample.svg.ep';
	my @files;
	my $dir   = $self->stash('config')->{tmp_dir};
	my $page  = 1;
	my $title = sprintf( "Generated at %s", DateTime->now( time_zone => 'local' )->strftime("%d %b %Y %T %z") );
	while (@hanzi) {
		my @h = splice( @hanzi, 0, 25 );

		my ( undef, $tmp ) = tempfile( 'sampleXXXXX', DIR => $dir );
		$mt->render_file_to_file( $tpl, $tmp,
			{ hanzi => \@h, title => $title, page => sprintf( "— %d —", $page++ ) } );
		system( "svg2pdf", $tmp, "$tmp.pdf" );
		unlink $tmp;
		push @files, "$tmp.pdf";
	}

	my $result;
	if ( @files > 1 ) {
		( undef, $result ) = tempfile( 'sampleXXXXX', DIR => $dir );
		unlink $result;
		$result .= ".pdf";
		system( qw/gs -q -sPAPERSIZE=a4 -dNOPAUSE -dBATCH -sDEVICE=pdfwrite/, "-sOutputFile=$result", @files );
		push @files, $result;
	} else {
		$result = $files[0];
	}
	$self->{tmp_files} ||= [];
	push @{ $self->{tmp_files} }, @files;

	$self->on(
		finish => sub {
			my $c = shift;
			unlink @{ $c->{tmp_files} };
			$c->{tmp_files} = [];
		}
	);

	my $static = Mojolicious::Static->new();
	$static->serve( $self, $result );

	$self->tx->resume;
	$self->rendered;
}

# word
sub word {
	my $self = shift;
	my $w = $self->dbh->selectrow_hashref( "SELECT * FROM words WHERE id=?", undef, $self->stash('word_id') );

	unless ( $w and %$w ) {
		$self->render_not_found;
		return;
	}

	$self->stash( w => $w );
	return 1;
}

sub show_word {
	my $self = shift;
	$self->stash( tags_tree => $self->dbh->get_tags_tree( $self->stash('w')->{id} ) );
	$self->render('words/show_word');
}

sub delete {
	my $self = shift;
	$self->dbh->do( "DELETE FROM words WHERE id=?", undef, $self->stash('w')->{id} );
	$self->redirect_to( $self->url_for('words_list') );
}

sub edit {
	my $self = shift;
	if ( $self->req->method eq 'POST' ) {
		trim( my $hanzi  = $self->param('hanzi') );
		trim( my $pinyin = $self->param('pinyin') );
		trim( my $yisi   = $self->param('yisi') );

		$self->dbh->do( "UPDATE words SET hanzi=?, pinyin=?, yisi=? WHERE id=?",
			undef, $hanzi, $pinyin, $yisi, $self->stash('w')->{id} );
		$self->redirect_to( $self->url_for( 'word_show', word_id => $self->stash('w')->{id} ) );
	} elsif ( $self->req->method eq 'GET' ) {
		$self->render( 'words/edit', %{ $self->stash('w') } );
	} else {
		$self->redirect_to( $self->url_for( 'word_edit', word_id => $self->stash('w')->{id} ) );
	}
}

sub add_word {
	my $self = shift;
	my %params = map { ( $_ => $self->param($_) ) } qw/hanzi pinyin yisi/;
	$self->dbh->do( "INSERT INTO words(hanzi, pinyin, yisi) VALUES (?, ?, ?)",
		undef, @params{qw/hanzi pinyin yisi/} );
	$self->redirect_to( $self->url_for( 'word_show', word_id => $self->dbh->last_insert_id( '', '', '', '' ) ) );
}

sub tags {
	my $self      = shift;
	my $tags_tree = $self->dbh->get_tags_tree( $self->stash('w')->{id} );
	$self->render( 'tags/tree', tags_tree => $tags_tree, word => $self->stash('w')->{id} );
}

sub tag_toggle {
	my $self = shift;

	trim( my $tag = $self->param('tag') );
	my $tag_id = $self->dbh->selectrow_array( "SELECT id FROM tags WHERE name=?", undef, $tag );
	my $word_id = $self->stash('w')->{id};
	if (
		$self->dbh->selectrow_array(
			"SELECT COUNT(*) FROM tags_words WHERE word=? AND tag=?",
			undef, $word_id, $tag_id
		)
		)
	{
		$self->dbh->do( "DELETE FROM tags_words WHERE word=? AND tag=?", undef, $word_id, $tag_id );
		$self->render( json => { result => 'deleted' } );
	} else {
		$self->dbh->do( "INSERT INTO tags_words(tag, word) VALUES (?, ?)", undef, $tag_id, $word_id );
		$self->render( json => { result => 'added' } );
	}
}

1;
