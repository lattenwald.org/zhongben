package ZhongBen;
use strict;
use warnings;

use Mojo::Base 'Mojolicious';
use Mojolicious::Plugin::Config;
use ZhongBen::Model;
use ZhongBen::Util;

my $dbh;

sub startup {
	my $self = shift;

	$self->secret(
		"My very secret secret, nobody aint gonna guess it, especially if I will not forget to add some mistekecei");
	$self->types->type( pdf => 'application/pdf' );

	my $config = $self->plugin('config');
	$dbh = ZhongBen::Model->new( $config->{db_connection} );
	$self->helper( dbh => sub { return $dbh } );
	$self->helper( pinyin_display => \&ZhongBen::Util::pin1yin1_display );

	my $r = $self->routes;

	# Words
	my $words_list = $r->bridge('/')->to( controller => 'words', action => 'list' );
	$words_list->route('')->to( action => 'show_list' )->name('words_list');
	$words_list->route('/sample')->to( controller => 'words', action => 'pdf_sample' )->name('pdf_sample');
	$r->route('/add')->via('get')->to( cb => sub { shift->render('words/add') } )->name('word_add');
	$r->route('/add')->via('post')->to( controller => 'words', action => 'add_word' );

	my $word = $r->bridge( '/:word_id', word_id => qr/\d+/ )->to( controller => 'words', action => 'word' );
	$word->route('')->to( action => 'show_word' )->name('word_show');
	$word->route('/edit')->to( action => 'edit' )->name('word_edit');
	$word->route('/delete')->via('post')->to( action => 'delete' )->name('word_delete');
	$word->route('/tree')->to( action => 'tags' )->name('word_tags_tree');
	$word->route('/tt')->to( action => 'tag_toggle' )->name('word_tag_toggle');

	# Tags
	my $tags = $r->route('/tag')->to( controller => 'tags' );
	$tags->route('/tree')->to( action => 'show_tags_tree' )->name('tag_tree');
	$tags->route('/new')->via('post')->to( action => 'new_tag' )->name('tag_new');
	$tags->route('/dropped')->via('post')->to( action => 'tag_dropped' )->name('tag_dropped');

	# Test
	my $test = $r->route('/t')->to( controller => 'test' );
	$test->route('/map')->to( action => 'replace_map' )->name('replace_map');
	$test->route('/t')->to( action => 'test' )->name('test');
}

1;
