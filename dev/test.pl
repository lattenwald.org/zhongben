#!/usr/bin/env perl
use strict;
use warnings;

use lib::abs '../lib';
use ZhongBen::Util;

# print "\$sre: $sre\n";
die "no input" unless @ARGV;

print pin1yin1_to_pinyin($ARGV[0]), "\n";
#print join ":", $ARGV[0] =~ /^($sre)+$/, "\n";
