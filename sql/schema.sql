-- each connection ought to do PRAGMA foreign_keys = ON

CREATE TABLE words (
		id INTEGER PRIMARY KEY AUTOINCREMENT,
		hanzi TEXT NOT NULL,
		pinyin TEXT NOT NULL,
		yisi TEXT NOT NULL DEFAULT ''
);

CREATE TABLE tags (
		id INTEGER PRIMARY KEY AUTOINCREMENT,
		parent INTEGER,
		name TEXT NOT NULL,
		FOREIGN KEY (parent) REFERENCES tags(id)
);

CREATE TABLE tags_words (
		word INTEGER,
		tag INTEGER,
		FOREIGN KEY (word) REFERENCES words(id),
		FOREIGN KEY (tag) REFERENCES tags(id),
		PRIMARY KEY (word, tag)
);